const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)
*/

function finditems(products){
    let res=Object.keys(
       Object.values((products)=>{
       products[0].filter(item=>item.price>65) ;

    } ))
    return res;
}
//console.log(finditems(products));

function quantityofitems(products){
    let res= Object.keys((products)=>{
    products[0].filter(item=>item.quantity>1);
    })
    return res;
    }
    //console.log(quantityofitems(products));

function itemsfragile(products){
    let res = Object.keys(
      products[0].filter(item=>item=="fragile") 
     )
    return res;
    }
   // console.log(itemsfragile(products));

